﻿using DuAn3.Models;
using DuAn3.Repository;
using DuAn3.Service;
using Training.UnitOfWork;

namespace DuAn3.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private StudentContext _context;

        public UnitOfWork(StudentContext context)
        {
            _context = context;
            IniRepositories();
        }


        public IGenericRepository<Class> classRepository { get; private set; }
        public IGenericRepository<Student> studentRepository { get; private set; }

        private void IniRepositories()
        {
            studentRepository = new GenericRepository<Student>(_context);
            classRepository = new GenericRepository<Class>(_context);
        }

      
        public void save()
        {
            _context.SaveChanges();
        }
    }
    
}
