﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DuAn3.Models;
using DuAn3.Repository;

namespace DuAn3.Service
{
    
    public interface IStudentService
    {
        Student GetById(int? Id);
        IEnumerable<Student> GetAll();
        Task Create(Student  student );
        void Delete(int Id);
        Task Update(Student student);
        void save();
    }
}
