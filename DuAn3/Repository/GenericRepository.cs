﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using DuAn3.Models;
using DuAn3.Service;
namespace DuAn3.Repository
{
    public class GenericRepository <TEntity> : IGenericRepository<TEntity> where  TEntity : class
    {
        private StudentContext _context;
        private DbSet<TEntity> Dbset;
        public GenericRepository(StudentContext context)
        {
            _context = context;
            this.Dbset = _context.Set<TEntity>();
            
        }

        public TEntity GetById(int? Id)
        {
            return Dbset.Find(Id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return  Dbset;
        }
//
        public async Task Create(TEntity tentity)
        {
             
            Dbset.Add(tentity);
             
        }

        public void Delete(int Id)
        {
            var tentity = ((IGenericRepository<TEntity>) this).GetById(Id);
            if (tentity != null)
            {
                Dbset.Remove(tentity);
              
            }
     
        }

        public async Task Update(TEntity tentity)
        {

            Dbset.Update(tentity);
        }

        public void save()
        {
            _context.SaveChanges();
        }
        
    }
}


