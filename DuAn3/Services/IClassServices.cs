﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DuAn3.Models;
using DuAn3.Repository;

namespace DuAn3.Service
{
    public interface  IClassService
    {
        Class GetById(int? Id);
        IEnumerable<Class> GetAll();
        Task Create(Class  classS );
        void Delete(int Id);
        Task Update(Class classS);
        void save();

    }
}
