﻿using DuAn3.Models;

namespace DuAn3.Repository
{
    public class StudentRepository : GenericRepository<Student>,IStudentRepository
    {
        public StudentRepository(StudentContext context) : base(context)
        {
        }
       
    }
}
