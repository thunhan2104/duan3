﻿using DuAn3.Models;

namespace DuAn3.Repository
{
    public class ClassRepository : GenericRepository<Class>, IClassRepository
    {
        public ClassRepository(StudentContext context) : base(context)
        {
        }
    }

}
