﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DuAn3.Models;
using DuAn3.Repository;

namespace DuAn3.Service
{
    public class ClassService : IClassService
    {

        private IClassRepository _classRepository;

        public ClassService(IClassRepository classRepository)
        {
            _classRepository = classRepository;
        }

        public Class GetById(int? Id)
        {
            return _classRepository.GetById(Id);
        }

        public IEnumerable<Class> GetAll()
        {
            return _classRepository.GetAll();
        }

        public Task Create(Class classS)
        {
            return _classRepository.Create(classS);
        }

        public void Delete(int Id)
        {
            _classRepository.Delete(Id);
        }

        public Task Update(Class classS)
        {
            return _classRepository.Update(classS);
        }

        public void save()
        {
            _classRepository.save();
        }
    }

}
