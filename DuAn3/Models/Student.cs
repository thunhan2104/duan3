﻿namespace DuAn3.Models
{
    public class Student
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string DiaChi { get; set; }
        public string Telephone { get; set; }
    }
}