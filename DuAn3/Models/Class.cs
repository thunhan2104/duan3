﻿using System.ComponentModel;

namespace DuAn3.Models
{
    public class Class
    {
        public int Id { get; set; }
        [DisplayName("Mã Lớp :")]
        public  string MaLop { get; set; }
        [DisplayName("Tên Lớp :")]
        public string TenLop { get; set; }
    }
}
