﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace DuAn3.Service
{
    public interface IGenericRepository<TEntity>
    {
        
        TEntity GetById(int? Id);
        IEnumerable<TEntity> GetAll();
        Task Create(TEntity tentity);
        void Delete(int Id);
        Task Update(TEntity tentity);
        void save();
    }
}
