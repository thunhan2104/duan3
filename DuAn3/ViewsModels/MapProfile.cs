﻿using AutoMapper;
namespace DuAn3.Services
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<Models.Student, ViewsModels.StudentViewsModels>();
        }
    }
    
}