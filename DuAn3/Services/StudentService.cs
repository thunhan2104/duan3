﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DuAn3.Models;
using Microsoft.EntityFrameworkCore;


namespace DuAn3.Service
{

    public interface IStudentService : IDisposable
    {
        IEnumerable<Student> GetStudents();
        Student GetStudentByID(int? studentId);
        void InsertStudent(Student student);
        void DeleteStudent(int studentID);
        void UpdateStudent(Student student);
        void Save();


    }

    public class StudentRepository : IStudentService, IDisposable
    {
        private readonly StudentContext _context;
        private readonly IMapper _mapper;

        public StudentRepository(StudentContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        private StudentContext context;

        public StudentRepository(StudentContext context)
        {
            this.context = context;
        }
        public IEnumerable<Student> GetStudents()
        {
            return _context.Students;
        }
 
        public Student GetStudentByID(int? id)
        {
            return _context.Students.Find(id);
        }
 
        public void InsertStudent(Student student)
        {
            _context.Students.Add(student);
            _context.SaveChanges();

        }
 
        public void DeleteStudent(int ID)
        {
            var student = _context.Students.Find(ID);
            _context.Students.Remove(student);
            _context.SaveChanges();
        }
 
        public void UpdateStudent(Student student)
        {
            _context.Entry(student).State = EntityState.Modified;
        }
 
        public void Save()
        {
            _context.SaveChanges();
        }
 
        private bool disposed = false;
 
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }
 
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
    
}

   