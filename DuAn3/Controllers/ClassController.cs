﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DuAn3.Models;
using DuAn3.Service;
using DuAn3.Service;

namespace DuAn3.Controllers
{
    public class ClassController : Controller
    {
        private readonly IClassService _classService;

        public ClassController (IClassService classService)
        {
            _classService = classService;
        }
       
        [HttpGet]
        public IActionResult Index()
        {
            return View(_classService.GetAll());
        }
       
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        
        [HttpPost]
        public async Task<IActionResult> Create(Class class1)
        {
            
            if(ModelState.IsValid)
            {
                _classService.Create(class1);
                _classService.save();
                return RedirectToAction("Index");
            }
            return RedirectToAction("Create");
        }

        [HttpGet]
       
        public IActionResult Delete(int Id)
        {
            _classService.Delete(Id);
            _classService.save();
            return RedirectToAction("Index");

        }
        [HttpGet]
        public IActionResult Edit(int? Id)
        { 
            return View(_classService.GetById(Id));
        }
        public async Task<IActionResult> Edit(Class class1)
        { 
            _classService.Update(class1);
            _classService.save();
            return RedirectToAction("Index");

        }
        
        
//        [HttpPost]
//        public async Task<IActionResult> GetId(int  Id)
//        {
//            var class1 = _context.GetId(Id);
//            if(class1==null)
//            {
//                return NotFound("Lỗi! Không tìm thay dữ liệu ");
//            }
//
//            return View(class1);
//        }
    }
}
