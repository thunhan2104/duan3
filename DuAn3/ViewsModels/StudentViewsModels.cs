﻿
namespace DuAn3.ViewsModels
{
    public class StudentViewsModels
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string DiaChi { get; set; }
        public string Telephone { get; set; }
        
    }
}