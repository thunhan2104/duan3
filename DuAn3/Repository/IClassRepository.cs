﻿using DuAn3.Models;
using DuAn3.Service;

namespace DuAn3.Repository
{
    public interface IClassRepository : IGenericRepository<Class>
    {
        
    }
}
