﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DuAn3.Services;
using DuAn3.Models;
using DuAn3.Service;
using DuAn3.ViewsModels;

namespace DuAn3.Controllers
{
    public class StudentController : Controller
    {
        private readonly IStudentService _studentService;

        public StudentController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        // GET getall
        public IActionResult GetAll()
        {
            return View(_studentService.GetAll());
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Student student)
        {
            _studentService.Create(student);
            return RedirectToAction(nameof(GetAll));
        }

//        [HttpGet]
//        public IActionResult Delete(int? id)
//        {
//            return View(_studentService.GetStudentByID(id));
//        }

        [HttpGet]
        public IActionResult Delete(int ID)
        {

            _studentService.Delete(ID);
            return RedirectToAction("GetAll");
        }

        [HttpGet]
        public IActionResult Edit(int? Id)
        {
            return View(_studentService.GetById(Id));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Student student)
        {
           
            _studentService.Update(student);
            return RedirectToAction("GetAll");
        }
}
}