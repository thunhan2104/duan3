﻿using DuAn3.Models;
using DuAn3.Repository;
using DuAn3.Service;

namespace Training.UnitOfWork
{
    public interface IUnitOfWork
    {
        IGenericRepository<Class> classRepository { get; }
        IGenericRepository<Student> studentRepository { get; }
        void save();

    }

    
}
