﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DuAn3.Models;
using DuAn3.Repository;

namespace DuAn3.Service
{
    public class StudentService : IStudentService
    {
        private IStudentRepository _studentRepository;

        public StudentService(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }
        public Student GetById(int? Id)
        {
            return _studentRepository.GetById(Id);
        }

        public IEnumerable<Student> GetAll()
        {
            return _studentRepository.GetAll();
        }

        public Task Create(Student student)
        {
            return _studentRepository.Create(student);
        }

        public void Delete(int Id)
        {
            _studentRepository.Delete(Id);
        }

        public Task Update(Student student)
        {
            return _studentRepository.Update(student);
        }

        public void save()
        {
            _studentRepository.save();
        }
    }
}
